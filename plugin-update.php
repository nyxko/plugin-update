<?php
/*
Plugin Name: Plugin Update
Description: Tests BitBucket plugin updates.
Version: 0.4.0
Author: Nicolae Pop
Author URI: https://nicolaepop.com
Text Domain: plugin-update
Domain Path: /languages

BitBucket Plugin URI: https://bitbucket.org/nyxko/plugin-update
*/

// This just echoes the chosen line, we'll position it later
function nico_plugin_update() {
	$version = get_plugin_data( __FILE__ )['Version'];
	echo "<p id='plugin-update'>plugin-update v$version</p>";
}

// Now we set that function up to execute when the admin_notices action is called
add_action( 'admin_notices', 'nico_plugin_update' );